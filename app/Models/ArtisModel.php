<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class ArtisModel extends Model
{
	protected $table = 't_master_artis';
	protected $primaryKey = 'id';

	protected $allowedFields = ['nama', 'created_by', 'foto', 'ext_a'];

	public function getAll($param = array())
	{
		if (isset($param['id'])) { $this->where('t_master_artis.id', $param['id']); }

		if (isset($param['list'])) { $this->select('
			t_master_artis.*,
			( SELECT GROUP_CONCAT( id_movie ) FROM t_movie_artis WHERE id_artis = t_master_artis.id ) AS id_filmnya,
			( SELECT IFNULL(GROUP_CONCAT((SELECT judul FROM t_movie WHERE id = id_movie)),"-") FROM t_movie_artis WHERE id_artis = t_master_artis.id ) AS judulnya'); }
		
		$query = $this->get();
		
		return $query;
	}

	public function addNew($data)
	{
		$data['query'] = $this->insert($data);
		$data['id'] = $this->insertID();

		return $data;
	}

	public function editAble($id, $data)
	{
		return $this->update($id, $data);
	}
}