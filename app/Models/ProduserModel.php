<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class ProduserModel extends Model
{
	protected $table = 't_master_produser';
	protected $primaryKey = 'id';

	protected $allowedFields = ['nama', 'created_by', 'foto', 'ext_p'];

	public function getAll($param = array())
	{
		if (isset($param['id'])) { $this->where('t_master_produser.id', $param['id']); }

		if (isset($param['list'])) { $this->select('
			t_master_produser.*,
			( SELECT GROUP_CONCAT( id_movie ) FROM t_movie_produser WHERE id_produser = t_master_produser.id ) AS id_filmnya,
			( SELECT IFNULL( GROUP_CONCAT( ( SELECT judul FROM t_movie WHERE id = id_movie ) ), "-" ) FROM t_movie_produser WHERE id_produser = t_master_produser.id ) AS judulnya ');}
		
		$query = $this->get();
		
		return $query;
	}

	public function addNew($data)
	{
		$data['query'] = $this->insert($data);
		$data['id'] = $this->insertID();

		return $data;
	}

	public function editAble($id, $data)
	{
		return $this->update($id, $data);
	}
}