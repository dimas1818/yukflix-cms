<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class TransaksiModel extends Model
{
	protected $table = 't_payment';
	protected $primaryKey = 'id';

	protected $allowedFields = ['userid', 'id_movie', 'id_provider', 'startpay', 'status'];


	public function getAll($param = array())
	{
		if (isset($param['id'])) { $this->where('id', $param['id']); }

		if (isset($param['list'])) {
			$this->select('t_payment.*, u.nama AS nama_user, u.email AS email_user, m.judul AS judul, m.harga AS tiket, mp.nama AS bayar_pakai');
			$this->join('t_user u','t_payment.userid = u.userid','left');
			$this->join('t_movie m','t_payment.id_movie = m.id','left');
			$this->join('t_master_provider mp','t_payment.id_provider = mp.id','left');
		}

		// $this->select('*');
		$query = $this->get();

		return $query;
	}

	public function addNew($data)
	{
		$query = $this->insert($data);

		return $query;
	}

	public function editAble($id, $data)
	{
		// $this->where('id', $id);
		return $this->update($id, $data);
	}
}