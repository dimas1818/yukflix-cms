<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class AuthModel extends Model
{
	protected $table = 't_login';
	protected $primaryKey = 'id';

	protected $allowedFields = ['username', 'password', 'role', 'created_by'];

	public function getAll($param = array())
	{
		if (isset($param['username'])) { $this->where('username', $param['username']); }
		if (isset($param['password'])) { $this->where('password', $param['password']); }
		if (isset($param['id'])) { $this->where('id', $param['id']); }
		if (isset($param['role'])) { $this->where('role', $param['role']); }
		
		$this->select('*');
		$query = $this->get();

		return $query;
	}
}