<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class FilmModel extends Model
{
	protected $table = 't_movie';
	protected $primaryKey = 'id';

	protected $allowedFields = ['judul', 'deskripsi', 'id_genre', 'tahun', 'trailer', 'trailerext','pathvideo', 'videoext','durasi', 'durasiwatch', 'harga', 'status', 'created_by', 'thumbnail', 'ext_t', 'cover', 'ext_c'];

	public function getAll($param = array())
	{
		if (isset($param['id'])) { $this->where('t_movie.id', $param['id']); }

		if (isset($param['list'])) {
			$this->select('t_movie.*, g.nama AS genre, d.id AS id_director, d.nama AS director, p.id AS id_produser, p.nama AS produser, (SELECT GROUP_CONCAT(id_artis) FROM t_movie_artis WHERE id_movie = t_movie.id) AS artis');
			$this->join('t_master_genre g', 't_movie.id_genre = g.id', 'left');
			$this->join('t_movie_director x', 't_movie.id = x.id_movie', 'left');
			$this->join('t_master_director d', 'x.id_director = d.id', 'left');
			$this->join('t_movie_produser xx', 't_movie.id = xx.id_movie', 'left');
			$this->join('t_master_produser p', 'xx.id_produser = p.id', 'left');
		}
		
		// $this->select('t_movie.*');
		$query = $this->get();
		
		return $query;
	}

	public function addNew($data)
	{
		$data['query'] = $this->insert($data);
		$data['id'] = $this->insertID();

		return $data;
	}

	public function editAble($id, $data)
	{
		return $this->update($id, $data);
	}
}