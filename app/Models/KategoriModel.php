<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class KategoriModel extends Model
{
	protected $table = 't_master_genre';
	protected $primaryKey = 'id';

	protected $allowedFields = ['nama', 'tampil', 'created_by'];


	public function getAll($param = array())
	{
		if (isset($param['id'])) { $this->where('id', $param['id']); }

		$this->select('*');
		$query = $this->get();

		return $query;
	}

	public function addNew($data)
	{
		$query = $this->insert($data);

		return $query;
	}

	public function editAble($id, $data)
	{
		// $this->where('id', $id);
		return $this->update($id, $data);
	}
}