<div class="row">
	<div class="col-12 col-sm-12 col-md-12">
		<div class="card" style="padding: 2%;">
			<div class="card-header">
				<button class="btn btn-info float-right add_kategori" id="add_kategori" type="button" data-toggle="modal"
					data-target="#addKategoriModal" data-backdrop="static" data-keyboard="false" style="margin-bottom: 1%;"><span
						class="fas fa-plus"> Tambah</span></button>
			</div>
			<table id="kategori_list" class="table table-bordered table-striped table-hover kategori_list" cellspacing="0"
				style="width: 100%;">
				<thead class="thead-light">
					<tr>
						<th style="vertical-align: top; text-align: center; font-size: 14px;">Nama Kategori</th>
						<th style="vertical-align: top; text-align: center; font-size: 14px;">Status</th>
						<th style="vertical-align: top; text-align: center; font-size: 14px;"></th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>

	<!-- Modal Tambah -->
		<?= view('modal/addKategori'); ?>
	<!-- End Tambah -->
</div>
