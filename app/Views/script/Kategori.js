$('#kategori_list').DataTable().destroy();
fill_datatable_kategori();
function fill_datatable_kategori(){
	$('#kategori_list').DataTable({
		"paging"		: true,
		"lengthChange"	: false,
		"searching"		: false,
		"ordering"		: false,
		"info"			: true,
		"autoWidth"		: false,
		"scrollX"		: true,
		"scrollY"		: true,
		"pageLength"	: 10,
		"ajax"			: {
			url		: "<?php echo base_url('kategori/list') ?>",
			type	: "GET",
		},
		"drawCallback": function (settings) { 
			// Here the response
			var response = settings.json;
			console.log(response);
		},
	});
}

// ADD
	$('#add_kategori_modal').submit(function(e){
	  if(!validateFormWajib()) return false;
	  var formData = new FormData(this);
	  e.preventDefault();
	  $.ajax({
	      type : "POST",
	      url  : "<?php echo site_url('kategori/add')?>",
	      data : formData,
	      processData:false,
	      contentType:false,
	      cache:false,
	      async:false,
	      success: function(data){
	      	if (data != null && data == 'Success') {
	      		$("#add_kategori_modal")[0].reset();
	      		$('#addKategoriModal').modal("hide");
	      		toastFire('success','Berhasil Tambah Kategori');
	      		$('#kategori_list').DataTable().destroy();
	      		fill_datatable_kategori();
	      	} else {
	      		toastFire('error','Terjadi Kesalahan..');
	      	}
	      },
	  });
	  return false;
	});
// END

// ENABLE
	$(document).on("click", ".enable_kategori", function () {
		// alert('Enabled!');
		var id_kat = $(this).data('iddc');
		// alert(id_kat);
		$.ajax({
	      type : "POST",
	      url  : "<?php echo site_url('kategori/enable')?>",
	      data : {id:id_kat},
	      success: function(data){
		    if (data != null && data == 'Success') {
		    	toastFire('success','Enabled');
		    	$('#kategori_list').DataTable().destroy();
		    	fill_datatable_kategori();``
		    } else {
		    	toastFire('error','Terjadi Kesalahan..');
		    }
	      },
	  });
	});
// END

// DISABLE
	$(document).on("click", ".disable_kategori", function () {
		// alert('Disabled!');
		var id_kat = $(this).data('iddc');
		// alert(id_kat);
		$.ajax({
	      type : "POST",
	      url  : "<?php echo site_url('kategori/disable')?>",
	      data : {id:id_kat},
	      success: function(data){
	      	if (data != null && data == 'Success') {
	      		toastFire('success','Disabled');
	      		$('#kategori_list').DataTable().destroy();
	      		fill_datatable_kategori();``
	      	} else {
	      		toastFire('error','Terjadi Kesalahan..');
	      	}
	      },
	  });
	});
// END