showartis();

function showartis(){
	$.ajax({
		type	: "GET",
		url		: "<?php echo site_url('artis/list'); ?>",
		dataType: 'json',
		success	: function(data) {
			for (var i = 0; i < data.length; i++) {
				var html = '<div class="col-xl-4 col-md-12">';
				html += '<div class="card card-profile">';
				html += '<div class="card-header card-header-image">';
				html += '<img class="img" src="../assets/kinarya/poto/pemain/'+data[i].foto+'/'+data[i].id+'.'+data[i].ext_a+'">';
				html += '<div class="colored-shadow" style="background-image: url(\'../assets/kinarya/poto/pemain/'+data[i].foto+'/'+data[i].id+'.'+data[i].ext_a+'\'); opacity: 1;"></div>';
				html += '</div>';
				html += '<div class="card-body ">';
				html += '<h4 class="card-category"><b>'+data[i].nama+'</b></h4>';
				html += '<span class="float-left">Film</span>';
				html += '<span class="tags float-right">';
				// html += '<h6 class="card-category">'+data[i].judulnya+'</h6>';
				html += '<span class="card-category">'+data[i].judulnya+'</span>';
				html += '</span>';
				html += '</div>';
				html += '<div class="card-footer justify-content-center">';
				html += '<button title="Edit" href="#pablo" class="btn btn-just-icon btn-warning btn-round" onclick="editArtis('+data[i].id+')">';
				html += '<i class="fas fa-pen-square"></i>';
				html += '<div class="ripple-container"></div>';
				html += '</button>';
				html += '</div>';
				html += '</div>';
				html += '</div>';

				$('#list_list').append(html);
			}
		}
	})
}

$('#fotoAktortris').hide();

function readURL(input,film) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#'+film+'').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#foto_aktortris").change(function() {
  $('#fotoAktortris').show();
  var foto = 'preview_aktortris';
  readURL(this,foto);
});

var save_method;

function addArtis(){
	save_method = 'add';
	$('#fotoAktortris').hide();
	document.getElementById('form_modal_artis').reset(); // reset form on modals

	$("#foto_aktortris").addClass("wajib_isi");
	$("#foto_aktortris").removeClass("is-invalid");
	$("#nama_aktortris").removeClass("is-invalid");
	$('#preview_aktortris').attr('src', '');

	$('#modalArtis').modal('show'); // show bootstrap modal
	$('.modal-title').text('Tambah Aktor /Aktris'); // Set Title to Bootstrap modal title
}

function editArtis(id){
	save_method = 'update';
	document.getElementById('form_modal_artis').reset(); // reset form on modals
	// alert('click');
	$.ajax({
		type	: "GET",
		url		: "<?php echo site_url('artis/list?id_artis=') ?>"+id,
		dataType: 'json',
		success : function(data){
			console.log(data);

			$("#foto_aktortris").removeClass("wajib_isi");
			$("#foto_aktortris").removeClass("is-invalid");
			$("#nama_aktortris").removeClass("is-invalid");
			$('#fotoAktortris').show();

			$('#id_artis').val(data.id);
			$('#nama_aktortris').val(data.nama);
			$('#foto').val(data.foto);
			$('#ext').val(data.ext_a);
			$('#preview_aktortris').attr('src', '../assets/kinarya/poto/pemain/'+data.foto+'/'+data.id+'.'+data.ext_a+'');

			$('#modalArtis').modal('show'); // show bootstrap modal
			$('.modal-title').text('Ubah Data Aktor /Aktris'); // Set Title to Bootstrap modal title
		}
	});
}

$('#form_modal_artis').submit(function(e){
  if(!validateFormWajib()) return false;
  var url;
  var toastsukses;
  var toastgagal;
  if (save_method == 'add') {
  	url = "<?php echo site_url('artis/add');?>";
  	toastsukses = "Berhasil Tambah Aktor / Aktris";
  	toastgagal = "Terjadi Kesalahan! Gagal Tambah Aktor / Aktris";
  } else {
  	url = "<?php echo site_url('artis/update');?>";
  	toastsukses = "Berhasil Ubah Data Aktor / Aktris";
  	toastgagal = "Terjadi Kesalahan! Gagal Ubah Data Aktor / Aktris";
  }

  var formData = new FormData(this);
  e.preventDefault();
  $.ajax({
      type : "POST",
      url  : url,
      data : formData,
      processData:false,
      contentType:false,
      cache:false,
      async:false,
      success: function(data){
      	if (data != null && data == "Success") {
      		document.getElementById("form_modal_artis").reset();
	      	$('#modalArtis').modal("hide");
	      	toastFire('success',toastsukses);
	      	$('#list_list').empty();
	      	showartis();
	    } else {
	    	toastFire('error',toastgagal);
	    }
      },
  });
  return false;
});