showmovies();

function showmovies(){
	$.ajax({
		type	: "GET",
		url		: "<?php echo site_url('film/list'); ?>",
		dataType: 'json',
		success	: function(data) {

			for (var i = 0; i < data.length; i++) {
				if (data[i].status == 1) {
					col = 'success';
					text = 'Release';
					// button = '<button type="button" class="unrelease_movie btn btn-block btn-outline-danger btn-xs" data-idmm="'+data[i].id+'">Unrelease</button>';
          button = '<button title="Unrelease" type="button" class="unrelease_movie btn btn-just-icon btn-danger btn-round" data-idmm="'+data[i].id+'"><i class="fas fa-times-circle" style="color:black;" ></i><div class="ripple-container"></div></button>';
				} else {
					col = 'warning';
					text = 'Coming Soon';
					// button = '<button type="button" id="release_movie" class="release_movie btn btn-block btn-outline-success btn-xs" data-idmm="'+data[i].id+'">Release</button>';
          button = '<button title="Release" type="button" id="release_movie" class="release_movie btn btn-just-icon btn-success btn-round" data-idmm="'+data[i].id+'"><i class="fas fa-check-circle" style="color:black;"></i><div class="ripple-container"></div></button>';
				}

        var html = '<div class="col-xl-4 col-md-12">';
        html += '<div class="card card-profile">';
        html += '<div class="card-header card-header-image">';
        html += '<img class="img" src="../assets/kinarya/poto/pilemnya/koper/'+data[i].cover+'/'+data[i].id+'.'+data[i].ext_c+'">';
        html += '<div class="colored-shadow" style="background-image: url(\'../assets/kinarya/poto/pilemnya/koper/'+data[i].cover+'/'+data[i].id+'.'+data[i].ext_c+'\'); opacity: 1;"></div>';
        html += '</div>';
        html += '<div class="card-body ">';
        html += '<h4 class="card-category"><b>'+data[i].judul+'</b></h4>';
        html += '<span class="float-left">Kategori Film</span>';
        html += '<span class="tags float-right">';
        html += '<span class="badge badge-pill badge-primary">'+data[i].genre+'</span>';
        html += '</span><br>';
        html += '<span class="float-left">Status</span>';
        html += '<span class="tags float-right">';
        html += '<span class="badge badge-pill badge-'+col+'">'+text+'</span>';
        html += '</span>';
        html += '</div>';
        html += '<div class="card-footer justify-content-center">';
        html += '<button title="Edit" class="btn btn-just-icon btn-warning btn-round" onclick="editMovies('+data[i].id+')">';
        html += '<i class="fas fa-pen-square"></i>';
        html += '<div class="ripple-container"></div>';
        html += '</button>';
        html += button;
        html += '</div>';
        html += '</div>';
        html += '</div>';

				$('#list_list').append(html);
			}
			// console.log(data.length);
		}
	})
}

$(document).on("click", ".release_movie", function () {
  var id_mov = $(this).data('idmm');
  $.ajax({
      method : "POST",
      url  : "<?php echo site_url('film/release')?>",
      data : {id:id_mov},
      success: function(data){
      	if(data != null && data == "Success"){
      		toastFire('success','Film Berhasil di Release!');
      		$('#list_list').empty();
      		showmovies();
      	} else {
      		toastFire('error','Terjadi Kesalahan!');
      	}
          
      },
  });
  return false;
});

$(document).on("click", ".unrelease_movie", function () {
  var id_mov = $(this).data('idmm');
  $.ajax({
      method : "POST",
      url  : "<?php echo site_url('film/unrelease')?>",
      data : {id:id_mov},
      success: function(data){
      	if(data != null && data == "Success"){
      		toastFire('success','Film Berhasil di Jadikan Coming Soon!');
      		$('#list_list').empty();
      		showmovies();
      	} else {
      		toastFire('error','Terjadi Kesalahan!');
      	}
          
      },
  });
  return false;
});


$('#trailerFilm').hide();
$('#_filmFilm').hide();
$('#preview_cover_main').hide();
$('#preview_thumbnail_main').hide();

function readURL(input,film) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#'+film+'').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#trailer_film").change(function() {
  $('#trailerFilm').show();
  var $source = $('#trailerPreview');
    $source[0].src = URL.createObjectURL(this.files[0]);
    $source.parent()[0].load();
});

$("#_film").change(function() {
  $('#_filmFilm').show();
  var $source = $('#filmPreview');
    $source[0].src = URL.createObjectURL(this.files[0]);
    $source.parent()[0].load();
});

$("#cover_film").change(function() {
  $('#preview_cover_main').show();
  var foto = 'preview_cover';
  readURL(this,foto);
});

$("#thumbnail_film").change(function() {
  $('#preview_thumbnail_main').show();
  var foto = 'preview_thumbnail';
  readURL(this,foto);
});

// Tahun
let startYear = 2000;
let endYear = new Date().getFullYear();
for (i = endYear; i > startYear; i--)
	{
      $('#tahun_film').append($('<option />').val(i).html(i));
    }

// Jam
let startJam = 1;
let endJam = 24;
for (i = startJam; i <= endJam; i++)
	{
      $('#jam_film').append($('<option />').val(i).html(i));
    }

// Menit
let startMenit = 0;
let endMenit = 60;
for (i = startMenit; i <= endMenit; i++)
	{
      $('#menit_film').append($('<option />').val(i).html(i));
    }

// Detik
let startDetik = 0;
let endDetik = 60;
for (i = startDetik; i <= endDetik; i++)
	{
      $('#detik_film').append($('<option />').val(i).html(i));
    }

var save_method;

function addMovies(){
	save_method = 'add';
	// $('#form_modal_movies').reset(); // reset form on modals
	document.getElementById('form_modal_movies').reset(); // reset form on modals
	$('#modalMovies').modal('show'); // show bootstrap modal
	$('.modal-title').text('Tambah Film'); // Set Title to Bootstrap modal title
}

function editMovies(id){
	save_method = 'update';
	$('#form_modal_movies').reset(); // reset form on modals

	$.ajax({
		type	: "GET",
		url		: "<?php echo site_url('film/list?id_movies='); ?>"+id,
		dataType: 'json',
		success : function(data){
			console.log(data);
		}
	});
}

$('#form_modal_movies').submit(function(e){
  if(!validateFormWajib()) return false;
  var url;
  var toastsukses;
  var toastgagal;
  if (save_method == 'add') {
  	url = "<?php echo site_url('film/add');?>";
  	toastsukses = "Berhasil Tambah Film";
  	toastgagal = "Terjadi Kesalahan! Gagal Tambah Film";
  } else {
  	url = "<?php echo site_url('film/update');?>";
  	toastsukses = "Berhasil Ubah Data Film";
  	toastgagal = "Terjadi Kesalahan! Gagal Ubah Data Film";
  }

  var formData = new FormData(this);
  e.preventDefault();
  // console.log(formData);
  $.ajax({
      type : "POST",
      url  : url,
      data : formData,
      processData:false,
      contentType:false,
      cache:false,
      async:false,
      success: function(data){
      	if (data != null && data == "Success") {
      		document.getElementById("form_modal_movies").reset();
      		$('#modalMovies').modal("hide");
      		toastFire('success',toastsukses);
      		$('#list_list').empty();
      		showmovies();
      	} else {
      		toastFire('error',toastgagal);
      	}
      }
  });
  return false;
});

$('#id_kategori').select2({
  placeholder: 'Pilih Kategori',
  allowClear: true
});

$('#tahun_film').select2({
  placeholder: 'Pilih Tahun',
  allowClear: true
});

$('#jam_film').select2({
  placeholder: 'Jam',
  allowClear: true
});

$('#menit_film').select2({
  placeholder: 'Menit',
  allowClear: true
});

$('#detik_film').select2({
  placeholder: 'Detik',
  allowClear: true
});

$('#prod_movies').select2({
  placeholder: 'Pilih Produser',
  allowClear: true
});

$('#dir_movies').select2({
  placeholder: 'Pilih Director',
  allowClear: true
});

$('#artis_movies1').select2({
  placeholder: 'Pilih Aktor/Aktris',
  allowClear: true
});