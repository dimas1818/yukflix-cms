showproduser();

function showproduser(){
	$.ajax({
		type	: "GET",
		url		: "<?php echo site_url('produser/list'); ?>",
		dataType: 'json',
		success	: function(data) {
			// console.log(data);
			for (var i = 0; i < data.length; i++) {
				var html = '<div class="col-xl-4 col-md-12">';
				html += '<div class="card card-profile">';
				html += '<div class="card-header card-header-image">';
				html += '<img class="img" src="../assets/kinarya/poto/produser/'+data[i].foto+'/'+data[i].id+'.'+data[i].ext_p+'">';
				html += '<div class="colored-shadow" style="background-image: url(\'../assets/kinarya/poto/produser/'+data[i].foto+'/'+data[i].id+'.'+data[i].ext_p+'\'); opacity: 1;"></div>';
				html += '</div>';
				html += '<div class="card-body ">';
				html += '<h4 class="card-category"><b>'+data[i].nama+'</b></h4>';
				html += '<span class="float-left">Film</span>';
				html += '<span class="tags float-right">';
				// html += '<h6 class="card-category">'+data[i].judulnya+'</h6>';
				html += '<span class="card-category">'+data[i].judulnya+'</span>';
				html += '</span>';
				html += '</div>';
				html += '<div class="card-footer justify-content-center">';
				html += '<button title="Edit" href="#pablo" class="btn btn-just-icon btn-warning btn-round" onclick="editProduser('+data[i].id+')">';
				html += '<i class="fas fa-pen-square"></i>';
				html += '<div class="ripple-container"></div>';
				html += '</button>';
				html += '</div>';
				html += '</div>';
				html += '</div>';

				$('#list_list').append(html);
			}
		}
	})
}

$('#fotoProduser').hide();

function readURL(input,film) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#'+film+'').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#foto_produser").change(function() {
  $('#fotoProduser').show();
  var foto = 'preview_produser';
  readURL(this,foto);
});

var save_method;

function addProduser(){
	save_method = 'add';
	$('#fotoProduser').hide();
	document.getElementById('form_modal_produser').reset(); // reset form on modals

	$("#foto_produser").addClass("wajib_isi");
	$("#foto_produser").removeClass("is-invalid");
	$("#nama_produser").removeClass("is-invalid");
	$('#preview_produser').attr('src', '');

	$('#modalProduser').modal('show'); // show bootstrap modal
	$('.modal-title').text('Tambah Produser'); // Set Title to Bootstrap modal title
}

function editProduser(id){
	save_method = 'update';
	document.getElementById('form_modal_produser').reset(); // reset form on modals
	// alert('click');
	$.ajax({
		type	: "GET",
		url		: "<?php echo site_url('produser/list?id_produser=') ?>"+id,
		dataType: 'json',
		success : function(data){
			console.log(data);

			$("#foto_produser").removeClass("wajib_isi");
			$("#foto_produser").removeClass("is-invalid");
			$("#nama_produser").removeClass("is-invalid");
			$('#fotoProduser').show();

			$('#id_produser').val(data.id);
			$('#nama_produser').val(data.nama);
			$('#foto').val(data.foto);
			$('#ext').val(data.ext_p);
			$('#preview_produser').attr('src', '../assets/kinarya/poto/produser/'+data.foto+'/'+data.id+'.'+data.ext_p+'');

			$('#modalProduser').modal('show'); // show bootstrap modal
			$('.modal-title').text('Ubah Data Produser'); // Set Title to Bootstrap modal title
		}
	});
}


$('#form_modal_produser').submit(function(e){
  if(!validateFormWajib()) return false;
  var url;
  var toastsukses;
  var toastgagal;
  if (save_method == 'add') {
  	url = "<?php echo site_url('produser/add');?>";
  	toastsukses = "Berhasil Tambah Produser";
  	toastgagal = "Terjadi Kesalahan! Gagal Tambah Produser";
  } else {
  	url = "<?php echo site_url('produser/update');?>";
  	toastsukses = "Berhasil Ubah Data Produser";
  	toastgagal = "Terjadi Kesalahan! Gagal Ubah Data Produser";
  }

  var formData = new FormData(this);
  e.preventDefault();
  $.ajax({
      type : "POST",
      url  : url,
      data : formData,
      processData:false,
      contentType:false,
      cache:false,
      async:false,
      success: function(data){
      	if (data != null && data == "Success") {
      		document.getElementById("form_modal_produser").reset();
	      	$('#modalProduser').modal("hide");
	      	toastFire('success',toastsukses);
	      	$('#list_list').empty();
	      	showproduser();
	    } else {
	    	toastFire('error',toastgagal);
	    }
      },
  });
  return false;
});