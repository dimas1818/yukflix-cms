showdirector();

function showdirector(){
	$.ajax({
		type	: "GET",
		url		: "<?php echo site_url('director/list'); ?>",
		dataType: 'json',
		success	: function(data) {
			// console.log(data);
			for (var i = 0; i < data.length; i++) {
				var html = '<div class="col-xl-4 col-md-12">';
				html += '<div class="card card-profile">';
				html += '<div class="card-header card-header-image">';
				html += '<img class="img" src="../assets/kinarya/poto/director/'+data[i].foto+'/'+data[i].id+'.'+data[i].ext_d+'">';
				html += '<div class="colored-shadow" style="background-image: url(\'../assets/kinarya/poto/director/'+data[i].foto+'/'+data[i].id+'.'+data[i].ext_d+'\'); opacity: 1;"></div>';
				html += '</div>';
				html += '<div class="card-body ">';
				html += '<h4 class="card-category"><b>'+data[i].nama+'</b></h4>';
				html += '<span class="float-left">Film</span>';
				html += '<span class="tags float-right">';
				// html += '<h6 class="card-category">'+data[i].judulnya+'</h6>';
				html += '<span class="card-category">'+data[i].judulnya+'</span>';
				html += '</span>';
				html += '</div>';
				html += '<div class="card-footer justify-content-center">';
				html += '<button title="Edit" href="#pablo" class="btn btn-just-icon btn-warning btn-round" onclick="editDirector('+data[i].id+')">';
				html += '<i class="fas fa-pen-square"></i>';
				html += '<div class="ripple-container"></div>';
				html += '</button>';
				html += '</div>';
				html += '</div>';
				html += '</div>';

				$('#list_list').append(html);
			}
		}
	})
}

$('#fotoDirector').hide();

function readURL(input,film) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#'+film+'').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#foto_director").change(function() {
  $('#fotoDirector').show();
  var foto = 'preview_director';
  readURL(this,foto);
});

var save_method;

function addDirector(){
	save_method = 'add';
	$('#fotoDirector').hide();
	document.getElementById('form_modal_director').reset(); // reset form on modals

	$("#foto_director").addClass("wajib_isi");
	$("#foto_director").removeClass("is-invalid");
	$("#nama_director").removeClass("is-invalid");
	$('#preview_director').attr('src', '');

	$('#modalDirector').modal('show'); // show bootstrap modal
	$('.modal-title').text('Tambah Director'); // Set Title to Bootstrap modal title
}

function editDirector(id){
	save_method = 'update';
	document.getElementById('form_modal_director').reset(); // reset form on modals
	// alert('click');
	$.ajax({
		type	: "GET",
		url		: "<?php echo site_url('director/list?id_director=') ?>"+id,
		dataType: 'json',
		success : function(data){
			console.log(data);

			$("#foto_director").removeClass("wajib_isi");
			$("#foto_director").removeClass("is-invalid");
			$("#nama_director").removeClass("is-invalid");
			$('#fotoDirector').show();

			$('#id_director').val(data.id);
			$('#nama_director').val(data.nama);
			$('#foto').val(data.foto);
			$('#ext').val(data.ext_d);
			$('#preview_director').attr('src', '../assets/kinarya/poto/director/'+data.foto+'/'+data.id+'.'+data.ext_d+'');

			$('#modalDirector').modal('show'); // show bootstrap modal
			$('.modal-title').text('Ubah Data Director'); // Set Title to Bootstrap modal title
		}
	});
}


$('#form_modal_director').submit(function(e){
  if(!validateFormWajib()) return false;
  var url;
  var toastsukses;
  var toastgagal;
  if (save_method == 'add') {
  	url = "<?php echo site_url('director/add');?>";
  	toastsukses = "Berhasil Tambah Director";
  	toastgagal = "Terjadi Kesalahan! Gagal Tambah Director";
  } else {
  	url = "<?php echo site_url('director/update');?>";
  	toastsukses = "Berhasil Ubah Data Director";
  	toastgagal = "Terjadi Kesalahan! Gagal Ubah Data Director";
  }

  var formData = new FormData(this);
  e.preventDefault();
  $.ajax({
      type : "POST",
      url  : url,
      data : formData,
      processData:false,
      contentType:false,
      cache:false,
      async:false,
      success: function(data){
      	if (data != null && data == "Success") {
      		document.getElementById("form_modal_director").reset();
	      	$('#modalDirector').modal("hide");
	      	toastFire('success',toastsukses);
	      	$('#list_list').empty();
	      	showdirector();
	    } else {
	    	toastFire('error',toastgagal);
	    }
      },
  });
  return false;
});