$('#transaksi_list').DataTable().destroy();
fill_datatable_transaksi();
function fill_datatable_transaksi(){
	$('#transaksi_list').DataTable({
		"paging"		: true,
		"lengthChange"	: false,
		"searching"		: false,
		"ordering"		: false,
		"info"			: true,
		"autoWidth"		: false,
		"scrollX"		: true,
		"scrollY"		: true,
		"pageLength"	: 10,
		"ajax"			: {
			url		: "<?php echo base_url('transaksi/list') ?>",
			type	: "GET",
		},
		"drawCallback": function (settings) { 
			// Here the response
			var response = settings.json;
			console.log(response);
		},
	});
}

$(document).on("click", ".konfirmasi_bayar", function () {
	// alert('Enabled!');
	var id_kat = $(this).data('idpp');
	// alert(id_kat);
	$.ajax({
      type : "POST",
      url  : "<?php echo site_url('transaksi/konfirmasi')?>",
      data : {id:id_kat},
      success: function(data){
	    if (data != null && data == 'Success') {
	    	toastFire('success','Berhasil Konfirmasi');
	    	$('#transaksi_list').DataTable().destroy();
	    	fill_datatable_transaksi();``
	    } else {
	    	toastFire('error','Terjadi Kesalahan..');
	    }
      },
  });
});