<div class="modal fade" id="modalMovies" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <form enctype="multipart/form-data" name="form" role="form" id="form_modal_movies">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <section class="content">
          <div class="container-fluid">
              <!-- kategori & nama -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Kategori Film</label>
                    <select class="form-control wajib_isi" id="id_kategori" name="id_kategori">
                      <?php foreach ($kategori as $key) : ?>
                        <option value="<?php echo $key->id;?>"><?php echo $key->nama;?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Nama Film</label>
                    <input class="form-control wajib_isi" id="nama_film" type="text" name="nama_film" placeholder="Nama Film" oninput="this.className = 'form-control wajib_isi'">
                  </div>
                </div>
              </div>
              <div class="dropdown-divider"></div>

              <!-- sinopsis -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Deskripsi Film</label>
                    <textarea class="form-control wajib_isi" id="deskripsi_film" type="text" name="deskripsi_film" oninput="this.className = 'form-control wajib_isi'" placeholder="Sinopsis Film"></textarea>
                  </div>
                </div>
              </div>
              <div class="dropdown-divider"></div>

              <!-- tahun -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Tahun</label>
                    <select class="form-control wajib_isi" id="tahun_film" name="tahun_film"></select>
                  </div>
                </div>
              </div>
              <div class="dropdown-divider"></div>

              <!-- durasi jam & menit -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Durasi</label>
                    <div class="row">
                      <div class="col-md-4">
                        <label>Jam</label>
                        <select class="form-control wajib_isi" id="jam_film" name="jam_film"></select>
                      </div>
                      <div class="col-md-4">
                        <label>Menit</label>
                        <select class="form-control wajib_isi" id="menit_film" name="menit_film"></select>
                      </div>
                      <div class="col-md-4">
                        <label>Detik</label>
                        <select class="form-control wajib_isi" id="detik_film" name="detik_film"></select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="dropdown-divider"></div>

              <!-- cover & thumbnail -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Cover Film</label>
                    <input class="form-control wajib_isi" id="cover_film" type="file" name="cover_film" accept="image/*" oninput="this.className = 'form-control wajib_isi'">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Thumbnail Film</label>
                    <input class="form-control wajib_isi" id="thumbnail_film" type="file" name="thumbnail_film" accept="image/*" oninput="this.className = 'form-control wajib_isi'">
                  </div>
                </div>
              </div>
              <div class="row" id="fotoFilm">
                <div class="col-md-6" id="preview_cover_main">
                  <div class="form-group">
                    <label>Preview Cover</label>
                    <img src="#" class="img-thumbnail" alt="your image" id="preview_cover"/>
                  </div>
                </div>
                <div class="col-md-6" id="preview_thumbnail_main">
                  <div class="form-group">
                    <label>Preview Thumbnail</label>
                    <img src="#" class="img-thumbnail" alt="your image" id="preview_thumbnail"/>
                  </div>
                </div>
              </div>
              <div class="dropdown-divider"></div>

              <!-- trailer -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Trailer Film</label>
                    <input class="form-control wajib_isi" id="trailer_film" type="file" name="trailer_film" accept="video/*" oninput="this.className = 'form-control wajib_isi'">
                  </div>
                </div>
              </div>
              <div class="row" id="trailerFilm">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Preview Trailer Film</label>
                    <div class="row">
                      <div class="col-md-12">
                        <video height="240" controls autoplay>
                          <source src="#" id="trailerPreview">
                          Your browser does not support the video tag.
                        </video>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="dropdown-divider"></div>

              <!-- film -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Film</label>
                    <input class="form-control wajib_isi" id="_film" type="file" name="_film" accept="video/*" oninput="this.className = 'form-control wajib_isi'">
                  </div>
                </div>
              </div>
              <div class="row" id="_filmFilm">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Preview Film</label>
                    <div class="row">
                      <div class="col-md-12">
                        <video height="240" controls autoplay>
                          <source src="#" id="filmPreview">
                          Your browser does not support the video tag.
                        </video>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="dropdown-divider"></div>

              <!-- pemeran -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Produser</label>
                    <select class="form-control wajib_isi" id="prod_movies" name="prod_movies">
                      <?php foreach ($produser as $key_prod) { ?>
                        <option value="<?php echo $key_prod->id;?>"><?php echo $key_prod->nama;?></span></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Director</label>
                    <select class="form-control wajib_isi" id="dir_movies" name="dir_movies">
                      <?php foreach ($director as $key_dir) { ?>
                        <option value="<?php echo $key_dir->id;?>"><?php echo $key_dir->nama;?></span></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Aktor/Aktris</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <select class="form-control wajib_isi" id="artis_movies1" name="artis_movies1[]" multiple="multiple">
                      <?php foreach ($artis as $key_artis) { ?>
                        <option value="<?php echo $key_artis->id;?>"><?php echo $key_artis->nama;?></span></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="dropdown-divider"></div>

              <!-- harga tiket -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Harga Tiket</label>
                    <input class="form-control wajib_isi uang" id="harga_tiket" type="text" name="harga_tiket" onkeypress="return isNumberKey(event)" onkeyup="splitInDots(this)" placeholder="Harga Tiket Dalam Rupiah" oninput="this.className = 'form-control wajib_isi uang'">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Status</label>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="icheck-primary d-inline">
                          <input type="radio" id="1" name="status_film" value="1" class="radioregis">
                          <label for="1">Release</label>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="icheck-primary d-inline">
                          <input type="radio" id="2" name="status_film" value="2" class="radioregis">
                          <label for="2">Coming Soon</label>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
          </div>
        </section>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="submit" id="submit_modal_movies" class="btn btn-info col-md-12">Simpan</button>
      </div>
    </div>
    </form>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>