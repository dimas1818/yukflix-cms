<div class="modal fade" id="modalArtis" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-md">
    <form enctype="multipart/form-data" name="form" role="form" id="form_modal_artis">
      <input type="hidden" name="id_artis" id="id_artis">
      <input type="hidden" name="foto" id="foto">
      <input type="hidden" name="ext" id="ext">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <section class="content">
            <div class="container-fluid">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Nama Aktor/Artis</label>
                      <input class="form-control wajib_isi" id="nama_aktortris" type="text" name="nama_aktortris" placeholder="Nama Aktor/Artis" oninput="this.className = 'form-control wajib_isi'">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Foto</label>
                      <input class="form-control wajib_isi" id="foto_aktortris" type="file" name="foto_aktortris" accept="image/*" oninput="this.className = 'form-control wajib_isi'">
                    </div>
                  </div>
                </div>
                <div class="row" id="fotoAktortris">
                  <div class="col-md-12" id="preview_foto_aktortris">
                    <div class="form-group">
                      <label>Preview Foto</label>
                      <img src="#" class="img-thumbnail" alt="foto" id="preview_aktortris"/>
                    </div>
                  </div>
                </div>
            </div>
          </section>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="submit" id="submit_modal_artis" class="btn btn-info col-md-12">Simpan</button>
        </div>
      </div>
    </form>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>