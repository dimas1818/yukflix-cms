<div class="modal fade" id="modalProduser" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-md">
    <form enctype="multipart/form-data" name="form" role="form" id="form_modal_produser">
      <input type="hidden" name="id_produser" id="id_produser">
      <input type="hidden" name="foto" id="foto">
      <input type="hidden" name="ext" id="ext">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <section class="content">
          <div class="container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Nama Produser</label>
                    <input class="form-control wajib_isi" id="nama_produser" type="text" name="nama_produser" placeholder="Nama Produser" oninput="this.className = 'form-control wajib_isi'">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Foto</label>
                    <input class="form-control wajib_isi" id="foto_produser" type="file" name="foto_produser" accept="image/*" oninput="this.className = 'form-control wajib_isi'">
                  </div>
                </div>
              </div>
              <div class="row" id="fotoProduser">
                <div class="col-md-12" id="preview_foto_produser">
                  <div class="form-group">
                    <label>Preview Foto</label>
                    <img src="#" class="img-thumbnail" alt="foto" id="preview_produser"/>
                  </div>
                </div>
              </div>
          </div>
        </section>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="submit" id="submit_modal_produser" class="btn btn-info col-md-12">Simpan</button>
      </div>
    </div>
    </form>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>