<aside class="main-sidebar elevation-4 sidebar-no-expand sidebar-light-danger">
    <!-- Brand Logo -->
    <a href="<?php echo base_url('dashboard'); ?>" class="brand-link text-sm navbar-danger">
	    <span class="brand-text font-weight-bold">YUKFLIX</span>
	</a>

	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column text-sm nav-flat nav-legacy nav-compact nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
				<li class="nav-item">
					<a href="<?php echo base_url('dashboard'); ?>" class="nav-link <?php if($title == 'Dashboard'): ?> active <?php endif; ?>">
						<i class="nav-icon fas fa-tachometer-alt"></i>
						<p>Dashboard</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo base_url('kategori'); ?>" class="nav-link <?php if($title == 'Kategori'): ?> active <?php endif; ?>">
						<i class="nav-icon fas fa-list-alt"></i>
						<p>Kategori Film</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo base_url('film'); ?>" class="nav-link <?php if($title == 'Film'): ?> active <?php endif; ?>">
						<i class="nav-icon fas fa-film"></i>
						<p>List Film</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
		            <a href="#" class="nav-link">
		            	<i class="nav-icon fas fa-users"></i>
		            	<p>
		            		List Pemeran
		            		<i class="right fas fa-angle-left"></i>
		            	</p>
		            </a>
		            <ul class="nav nav-treeview" style="display: none;">
		            	<li class="nav-item">
		                	<a href="<?php echo base_url('artis'); ?>" class="nav-link <?php if($title == 'Aktor / Aktris'): ?> active <?php endif; ?>">
		                  		<i class="fas fa-user-astronaut nav-icon"></i>
		                  		<p>Aktor/Aktris</p>
		                	</a>
		            	</li>
	            		<li class="nav-item">
	            	    	<a href="<?php echo base_url('director'); ?>" class="nav-link <?php if($title == 'Director'): ?> active <?php endif; ?>">
	            	      		<i class="fas fa-user-secret nav-icon"></i>
	            	      		<p>Director</p>
	            	    	</a>
	            	  	</li>
		            	<li class="nav-item">
		                	<a href="<?php echo base_url('produser'); ?>" class="nav-link <?php if($title == 'Produser'): ?> active <?php endif; ?>">
		                		<i class="fas fa-user-tie nav-icon"></i>
		                		<p>Produser</p>
		                	</a>
		            	</li>
		            </ul>
		        </li>
				<li class="nav-item">
					<a href="<?php echo base_url('transaksi'); ?>" class="nav-link <?php if($title == 'Transaksi'): ?> active <?php endif; ?>">
						<i class="nav-icon fas fa-list-alt"></i>
						<p>Transaksi</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo base_url('user'); ?>" class="nav-link <?php if($title == 'User'): ?> active <?php endif; ?>">
						<i class="nav-icon fas fa-list-alt"></i>
						<p>User</p>
					</a>
				</li>
			</ul>
		</nav>
		<!--/ .Sidebar Menu -->
	</div>
	<!--/ .Sidebar -->
</aside>