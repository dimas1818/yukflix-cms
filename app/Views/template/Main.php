<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<title>YUKFLIX ADMIN | <?= $title; ?></title>
    	<!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Tempusdominus Bbootstrap 4 -->
        <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4-4.min.css">
        <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/plugins/select2/css/select2.min.css">
        <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- JQVMap -->
        <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/plugins/jqvmap/jqvmap.min.css">
        <!-- SweetAlert2 -->
        <!-- <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/plugins/sweetalert2/sweetalert2.min.css"> -->
        <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
        <!-- Toastr -->
        <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/plugins/toastr/toastr.min.css">
        <!-- Datatables -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('public/AdminLTE'); ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <!-- Toastr -->
        <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/plugins/toastr/toastr.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/dist/css/adminlte.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="<?= base_url('public/AdminLTE'); ?>/plugins/daterangepicker/daterangepicker.css">

        <style type="text/css">
            .bd-example-modal-lg .modal-dialog{
                display: table;
                position: relative;
                margin: 0 auto;
                top: calc(50% - 24px);
              }
              
              .bd-example-modal-lg .modal-dialog .modal-content{
                background-color: transparent;
                border: none;
              }

              <?php if ($style != null || $style != '') {
                  echo view($style);
              } ?>

        </style>
    </head>
    <body class="sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed control-sidebar-slide-open text-sm accent-navy">
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand text-sm navbar-dark navbar-danger">
                <!-- Left Navbar Links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>
                <!-- Right Navbar Links -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown show">
                        <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="true">
                            <i class="fas fa-user-cog fa-md" style="color: black;"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <span class="dropdown-item dropdown-header">Hello <?= $session_nama; ?></span>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item" id="add_user" type="button" data-toggle="modal" data-target="#editProfileModal">
                              <i class="fas fa-user-edit mr-2"></i> Edit Profil
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= base_url('logout'); ?>" class="dropdown-item" id="log_out" type="button">
                              <i class="fas fa-power-off mr-2"></i> Log Out
                            </a>
                        </div>
                    </li>
                </ul>
            </nav>
            <!--/ .Navbar  -->

            <!-- Main Sidebar Container -->
            <?= view('template/Sidebar'); ?>
            <!--/ .Main Sidebar Container -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="min-height: 550px;">
                <!-- Contetn Header (Page Header)-->
                <div class="content-header">
                    <div class="container-fluid" style="background-color: white;padding: 1%;">
                        <div class="row">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark"><?= $title; ?></h1>
                            </div>
                            <!--/ .col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
                                    </li>
                                    <?php if($title != 'Dashboard'): ?>
                                        <li class="breadcrumb-item active"><?= $title; ?></li>
                                    <?php endif; ?>
                                </ol>
                            </div>
                            <!--/ .col -->
                        </div>
                        <!--/ .row -->
                    </div>
                    <!--/ .container-fluid -->
                </div>
                <!--/ .Contetn Header -->

                <!-- Main Content -->
                <section class="content">
                    <div class="container-fluid">
                        <?= view($view); ?>
                    </div>
                    <!--/ .container-fluid-->
                </section>
                <!--/ .Main Content -->
            </div>
            <!--/ .Content Wrapper. Contains page content -->

        </div>


        <!-- REQUIRED SCRIPTS -->
        <!-- jQuery -->
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/jquery/jquery.min.js"></script>
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
        <!-- Bootstrap 4-->
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Select2 -->
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/select2/js/select2.full.min.js"></script>
        <!-- SweetAlert2 -->
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/sweetalert2/sweetalert2.all.min.js"></script>
        <!-- Toastr -->
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/toastr/toastr.min.js"></script>
        <!-- Toastr -->
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/toastr/toastr.min.js"></script>
        <!-- Input Mask -->
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/moment/moment.min.js"></script>
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
        <!-- overlayScrollbars -->
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <!-- Tempudominus Bootstrap4 -->
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?= base_url('public/AdminLTE'); ?>/dist/js/adminlte.js"></script>
        <!-- ChartJS -->
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/chart.js/Chart.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
        <!-- DataTables -->
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
        <!-- DateRangePicker -->
        <script src="<?= base_url('public/AdminLTE'); ?>/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- Popper.JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

        <script type="text/javascript">
            
            function validateFormWajib() {
              // This function deals with validation of the form fields
              var y, i, valid = true;
              y = document.getElementsByClassName("wajib_isi");
              // A loop that checks every input field in the current tab:
              for (i = 0; i < y.length; i++) {
                // If a field is empty...
                if (y[i].value == "") {
                  // add an "invalid" class to the field:
                  y[i].className += " is-invalid";
                  // and set the current valid status to false:
                  valid = false;
                }
              }
              return valid; // return the valid status
            }

            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            function toastFire(status, message) {
                Toast.fire({
                    type: status,
                    title: message
              });
            }

            function alerttoast(message){
                toastr.error(message);
            }

            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : evt.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            function reverseNumber(input) {
                return [].map.call(input, function(x) {
                    return x;
                }).reverse().join('');
            }

            function plainNumber(number) {
                return number.split('.').join('');
            }

            function splitInDots(input) {

                var value = input.value,
                    plain = plainNumber(value),
                    reversed = reverseNumber(plain),
                    reversedWithDots = reversed.match(/.{1,3}/g).join('.'),
                    normal = reverseNumber(reversedWithDots);

                console.log(plain, reversed, reversedWithDots, normal);
                input.value = normal;
            }

            function oneDot(input) {
                var value = input.value,
                    value = plainNumber(value);

                if (value.length > 3) {
                    value = value.substring(0, value.length - 3) + '.' + value.substring(value.length - 3, value.length);
                }
                console.log(value);
                input.value = value;
            }

            <?php if ($js != null || $js != '') {
                echo view($js);
            } ?>
        </script>
    </body>
</html>
