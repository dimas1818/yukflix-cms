<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
	<title>YUKFLIX | Admin - Login</title>
   <!--Made with love by Mutiullah Samim -->

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url('public/AdminLTE'); ?>/plugins/fontawesome-free/css/all.min.css">

	<!--Theme style-->
	<link rel="stylesheet" href="<?php echo base_url('public/AdminLTE'); ?>/dist/css/adminlte.min.css">

	<!-- SweetAlert2 -->
	<link rel="stylesheet" href="<?php echo base_url('public/AdminLTE'); ?>/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

	<!-- Toastr -->
	<link rel="stylesheet" href="<?php echo base_url('public/AdminLTE'); ?>/plugins/toastr/toastr.min.css">

	

	<style type="text/css">
		/* Made with love by Mutiullah Samim*/

		/*@import url('https://fonts.googleapis.com/css?family=Numans');*/

		html,body{
		/*background-image: url('http://getwallpapers.com/wallpaper/full/a/5/d/544750.jpg');*/
		background-image: url('<?php echo base_url()."/public/bcg.jpg"; ?>');
		background-size: cover;
		background-repeat: no-repeat;
		height: 100%;
		font-family: 'Numans', sans-serif;
		}

		.container{
		height: 100%;
		align-content: center;
		}

		.card{
		/*height: 370px;*/
		margin-top: auto;
		margin-bottom: auto;
		width: 400px;
		background-color: rgba(0,0,0,0.5) !important;
		}

		.social_icon span{
		font-size: 60px;
		margin-left: 10px;
		color: #dc3545;
		}

		.social_icon span:hover{
		color: white;
		cursor: pointer;
		}

		.card-header h3{
		color: white;
		}

		.social_icon{
		position: absolute;
		right: 20px;
		top: -45px;
		}

		.input-group-prepend span{
		width: 50px;
		background-color: #dc3545;
		color: black;
		border:0 !important;
		}

		input:focus{
		outline: 0 0 0 0  !important;
		box-shadow: 0 0 0 0 !important;

		}

		.remember{
		color: white;
		}

		.remember input
		{
		width: 20px;
		height: 20px;
		margin-left: 15px;
		margin-right: 5px;
		}

		.login_btn{
		color: black;
		background-color: #dc3545;
		width: 100px;
		}

		.login_btn:hover{
		color: black;
		background-color: white;
		}

		.links{
		color: white;
		}

		.links a{
		margin-left: 4px;
		}
	</style>
</head>
<body>
<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h3>YUKFLIX</h3>
				<!-- <div class="d-flex justify-content-end social_icon">
					<span><i class="fab fa-facebook-square"></i></span>
					<span><i class="fab fa-google-plus-square"></i></span>
					<span><i class="fab fa-twitter-square"></i></span>
				</div> -->
			</div>
			<div class="card-body">
				<form enctype="multipart/form-data" name="form" role="form" id="login_form">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="text" class="form-control wajib_isi" name="username" placeholder="username" oninput="this.className = 'form-control wajib_isi'">
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" class="form-control wajib_isi" name="password" placeholder="password" oninput="this.className = 'form-control wajib_isi'">
					</div>
					<!-- <div class="row align-items-center remember">
						<input type="checkbox">Remember Me
					</div> -->
					<div class="form-group">
						<input type="submit" value="Login" class="btn float-right login_btn">
					</div>
				</form>
			</div>
			<!-- <div class="card-footer">
				<div class="d-flex justify-content-center links">
					Don't have an account?<a href="#">Sign Up</a>
				</div>
				<div class="d-flex justify-content-center">
					<a href="#">Forgot your password?</a>
				</div>
			</div> -->
		</div>
	</div>
</div>
<!-- jQuery -->
<script src="<?php echo base_url('public/AdminLTE'); ?>/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url('public/AdminLTE'); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Bootstrap 4-->
<script src="<?php echo base_url('public/AdminLTE'); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('public/AdminLTE'); ?>/dist/js/adminlte.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url('public/AdminLTE'); ?>/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url('public/AdminLTE'); ?>/plugins/toastr/toastr.min.js"></script>
<script type="text/javascript">
	function validateFormWajib() {
	  // This function deals with validation of the form fields
	  var y, i, valid = true;
	  y = document.getElementsByClassName("wajib_isi");		  // A loop that checks every input field in the current tab:
	  for (i = 0; i < y.length; i++) {
	    // If a field is empty...
	    if (y[i].value == "" || y[i].value == null) {
	      // add an "invalid" class to the field:
	      y[i].className += " is-invalid";
	      // and set the current valid status to false:
	      valid = false;
	    }
	  }
	  return valid; // return the valid status
	}

	const Toast = Swal.mixin({
	    toast: true,
	    position: 'top-end',
	    showConfirmButton: false,
	    timer: 3000
	});

	function toastFire(status, message) {
	    Toast.fire({
	        type: status,
	        title: message
	  });
	}

	$('#login_form').submit(function(e){
	  if(!validateFormWajib()) return false;
	  var formData = new FormData(this);
	  e.preventDefault();
	  $.ajax({
	      type : "POST",
	      url  : "<?php echo site_url('login')?>",
	      data : formData,
	      processData:false,
	      contentType:false,
	      cache:false,
	      async:false,
	      success: function(data){
		      	if (data != null && data == "Success") {
		      		window.location.replace('dashboard');
		      	} else {
		      		// alert(data);
		      		toastFire('error','Username or Password is Wrong!');;
		      	}
	      },
	  });
	  return false;
	});
</script>
</body>
</html>