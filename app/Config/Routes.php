<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Auth');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Auth::index');
$routes->get('one', 'Auth::index');
$routes->post('login', 'Auth::login');
$routes->get('logout', 'Auth::logoutSession');

$routes->get('dashboard', 'Dashboard::index');

$routes->get('kategori', 'Kategori::index');
$routes->get('kategori/list', 'Kategori::list');
$routes->post('kategori/add', 'Kategori::add');
$routes->post('kategori/enable', 'Kategori::enable');
$routes->post('kategori/disable', 'Kategori::disable');


$routes->get('film', 'Film::index');
$routes->get('film/list', 'Film::list');
$routes->post('film/add', 'Film::add');
$routes->post('film/release', 'Film::release');
$routes->post('film/unrelease', 'Film::unrelease');
$routes->post('film/delete', 'Film::delete');

$routes->get('artis', 'Artis::index');
$routes->get('artis/list', 'Artis::list');
$routes->get('artis/add', 'Artis::add');
$routes->get('artis/update', 'Artis::update');

$routes->get('produser', 'Produser::index');
$routes->get('produser/list', 'Produser::list');
$routes->get('produser/add', 'Produser::add');
$routes->get('produser/update', 'Produser::update');

$routes->get('transaksi', 'Transaksi::index');
$routes->get('transaksi/list', 'Transaksi::list');
$routes->get('transaksi/konfirmasi', 'Transaksi::konfirmasi');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
