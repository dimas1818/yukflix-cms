<?php
function judul_film($id){
	$db      = \Config\Database::connect();
	$builder = $db->table('t_movie');

	$ret = $builder->get();
	$ret_ret = $ret->getRow();
	$return = $ret_ret->judul;
	return $return;
}

function artisnya($id){
	$db      = \Config\Database::connect();
	$builder = $db->table('t_master_artis');

	$ret = $builder->get();
	$ret_ret = $ret->getRow();
	$return = $ret_ret->nama;
	return $return;
}