<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\AuthModel;
 
class Auth extends Controller
{
	public function __construct()
	{
		helper('security');
        $this->session = session();
        $this->auth = new AuthModel();

    }

    public function index()
    {	
    	if ($this->session->has('sess_masuk')) {
    		return redirect()->route('dashboard');
    	} else {
    		return view('auth/Login');
    	}
    }

    public function login()
    {
    	if ($this->session->has('sess_masuk')) {
    		return redirect()->route('dashboard');
    	} else {
    		$username = $this->request->getPost('username');
    		$password = do_hash($this->request->getPost('password'));

    		$param_cek = array('username' => $username, 'password' => $password);
    		$auth_cek = $this->auth->getAll($param_cek);

    		if (count($auth_cek->getResult()) == 0) {
    			$data = "Failed";
    		} else {
    			$auth_in = $auth_cek->getRowArray();
    			$in = date('Y-m-d H:i:s');
    			
    			$sessArray = [
    				'sess_masuk'	=>	TRUE,
    				'sess_name'		=>	'Admin',
    				'sess_id'		=>	$auth_in['id'],
    				'sess_inTime'	=>	$in
    			];
    			$this->session->set($sessArray);
    			
    			$data = "Success";
    		}

    		echo $data;
    		exit();
    	}
    }

    public function getSession()
    {
    	if ($this->session->has('sess_masuk')) {
    		if ($this->session->get('sess_masuk') === TRUE) {
    			echo $this->session->get('sess_name');
    		} else {
    			echo $this->session->get('sess_masuk');
    		}
    	} else {
    		echo "No session set";
    	}
    }

    public function logoutSession()
    {
    	$this->session->destroy();

        return redirect()->route('one');
    }
}