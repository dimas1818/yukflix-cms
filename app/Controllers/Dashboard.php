<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\AuthModel;
 
class Dashboard extends Controller
{
	public function __construct()
	{
		helper('security');
        $this->session = session();
        $this->auth = new AuthModel();
    }

    public function index()
    {
    	if (! $this->session->has('sess_masuk')) {
    		return redirect()->route('one');
    	} else {
    		$data['title']			=	'Dashboard';
    		$data['view']			=	'admin/Dashboard';
    		$data['js']				=	'';
            $data['style']          =   '';
    		$data['session_nama']	=	$this->session->get('sess_name');
    		
    		return view('template/Main', $data);
    	}
    }
}