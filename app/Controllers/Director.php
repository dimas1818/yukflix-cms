<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\AuthModel;
use App\Models\FilmModel;
use App\Models\DirectorModel;
 
class Director extends Controller
{
	public function __construct()
	{
		helper('security');
        helper('text_helper');
        $this->session = session();
        $this->auth = new AuthModel();
        $this->director = new DirectorModel();
        $this->film = new FilmModel();
    }

    public function index()
    {
    	if (! $this->session->has('sess_masuk')) {
    		return redirect()->route('one');
    	} else {
    		$data['title']			=	'Director';
    		$data['view']			=	'admin/Director';
    		$data['js']				=	'script/Director.js';
            $data['style']          =   'style/Director.css';
    		$data['session_nama']	=	$this->session->get('sess_name');
    		
    		return view('template/Main', $data);
    	}
    }

    public function list()
    {
        if (! $this->session->has('sess_masuk')) {
            return redirect()->route('one');
        } else {
            $param  =   array('list'  => 1);
            $data   =   $this->director->getAll($param)->getResult();

            if ($this->request->getGet('id_director')) {
                $param =    array_merge($param, array('id' =>  $this->request->getGet('id_director')));
                $data  =    $this->director->getAll($param)->getRow();
            }

            echo json_encode($data);
            exit();
        }
    }

    public function add()
    {
        if (! $this->session->has('sess_masuk')) {
            return redirect()->route('one');
        } else {
            $nama_director         = $this->request->getPost('nama_director');
            $foto_director         = $this->request->getFile('foto_director');

            $data_add_director = array(
                'nama'          => $nama_director,
                'created_by'    => $this->session->get('sess_id')
            );

            if (! $add_director = $this->director->addNew($data_add_director)) {
                $data = "Failed";
            } else {
                $new_id_director = $add_director['id'];

                // $lokasiDirector     = ROOTPATH.'writable/uploads/images/actor-ess/';
                $lokasiDirector    = '../assets/kinarya/poto/director/';

                $acak = random_string('alnum', 10).$new_id_director;

                $createDirector        = $lokasiDirector.$acak;
                
                if(!mkdir($createDirector,0777)){
                    $data = "Failed";
                }

                $ext_d = $foto_director->getExtension();
                // $foto_director->store('images/actor-ess/'.$acak,$new_id_director.'.'.$ext_d);
                $foto_director->move($createDirector,$new_id_director.'.'.$ext_d);

                $data_up_director = array(
                    'foto'  => $acak,
                    'ext_d' => $ext_d
                );

                if (! $this->director->editAble($new_id_director, $data_up_director)) {
                    $data = "Failed";
                } else {
                    $data = "Success";
                }
            }

            echo $data;
            exit();
        }
    }

    public function update()
    {
        if (! $this->session->has('sess_masuk')) {
            return redirect()->route('one');
        } else {
            $id_director           = $this->request->getPost('id_director');
            $nama_director         = $this->request->getPost('nama_director');
            $foto_director         = $this->request->getFile('foto_director');
            $foto                   = $this->request->getPost('foto');
            $ext                    = $this->request->getPost('ext');

            if ($foto_director == null || $foto_director == '') {
                $data_up_director = array(
                    'nama'  => $nama_director
                );
            } else {
                $lokasi     = '../assets/kinarya/poto/director/'.$foto.'/';
                $_old       = $lokasi.$id_director.'.'.$ext.'';

                if (file_exists($_old)) {
                    if (! unlink($_old)) {
                        $data = "Failed";
                    } else {
                        $ext_d = $foto_director->getExtension();
                        $foto_director->move($lokasi,$id_director.'.'.$ext_d);

                        $data_up_director = array(
                            'nama'  => $nama_director,
                            'ext_d' => $ext_d
                        );
                    }
                }
            }

            if (! $this->director->editAble($id_director, $data_up_director)) {
                $data = "Failed";
            } else {
                $data = "Success";
            }

            echo $data;
            exit();
        }
    }
}