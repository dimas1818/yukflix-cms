<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\AuthModel;
use App\Models\FilmModel;
use App\Models\ProduserModel;
 
class Produser extends Controller
{
	public function __construct()
	{
		helper('security');
        helper('text_helper');
        $this->session = session();
        $this->auth = new AuthModel();
        $this->produser = new ProduserModel();
        $this->film = new FilmModel();
    }

    public function index()
    {
    	if (! $this->session->has('sess_masuk')) {
    		return redirect()->route('one');
    	} else {
    		$data['title']			=	'Produser';
    		$data['view']			=	'admin/Produser';
    		$data['js']				=	'script/Produser.js';
            $data['style']          =   'style/Produser.css';
    		$data['session_nama']	=	$this->session->get('sess_name');
    		
    		return view('template/Main', $data);
    	}
    }

    public function list()
    {
        if (! $this->session->has('sess_masuk')) {
            return redirect()->route('one');
        } else {
            $param  =   array('list'  => 1);
            $data   =   $this->produser->getAll($param)->getResult();

            if ($this->request->getGet('id_produser')) {
                $param =    array_merge($param, array('id' =>  $this->request->getGet('id_produser')));
                $data  =    $this->produser->getAll($param)->getRow();
            }

            echo json_encode($data);
            exit();
        }
    }

    public function add()
    {
        if (! $this->session->has('sess_masuk')) {
            return redirect()->route('one');
        } else {
            $nama_produser         = $this->request->getPost('nama_produser');
            $foto_produser         = $this->request->getFile('foto_produser');

            $data_add_produser = array(
                'nama'          => $nama_produser,
                'created_by'    => $this->session->get('sess_id')
            );

            if (! $add_produser = $this->produser->addNew($data_add_produser)) {
                $data = "Failed";
            } else {
                $new_id_produser = $add_produser['id'];

                // $lokasiProduser     = ROOTPATH.'writable/uploads/images/actor-ess/';
                $lokasiProduser    = '../assets/kinarya/poto/produser/';

                $acak = random_string('alnum', 10).$new_id_produser;

                $createProduser        = $lokasiProduser.$acak;
                
                if(!mkdir($createProduser,0777)){
                    $data = "Failed";
                }

                $ext_p = $foto_produser->getExtension();
                // $foto_produser->store('images/actor-ess/'.$acak,$new_id_produser.'.'.$ext_p);
                $foto_produser->move($createProduser,$new_id_produser.'.'.$ext_p);

                $data_up_produser = array(
                    'foto'  => $acak,
                    'ext_p' => $ext_p
                );

                if (! $this->produser->editAble($new_id_produser, $data_up_produser)) {
                    $data = "Failed";
                } else {
                    $data = "Success";
                }
            }

            echo $data;
            exit();
        }
    }

    public function update()
    {
        if (! $this->session->has('sess_masuk')) {
            return redirect()->route('one');
        } else {
            $id_produser           = $this->request->getPost('id_produser');
            $nama_produser         = $this->request->getPost('nama_produser');
            $foto_produser         = $this->request->getFile('foto_produser');
            $foto                   = $this->request->getPost('foto');
            $ext                    = $this->request->getPost('ext');

            if ($foto_produser == null || $foto_produser == '') {
                $data_up_produser = array(
                    'nama'  => $nama_produser
                );
            } else {
                $lokasi     = '../assets/kinarya/poto/produser/'.$foto.'/';
                $_old       = $lokasi.$id_produser.'.'.$ext.'';

                if (file_exists($_old)) {
                    if (! unlink($_old)) {
                        $data = "Failed";
                    } else {
                        $ext_p = $foto_produser->getExtension();
                        $foto_produser->move($lokasi,$id_produser.'.'.$ext_p);

                        $data_up_produser = array(
                            'nama'  => $nama_produser,
                            'ext_p' => $ext_p
                        );
                    }
                }
            }

            if (! $this->produser->editAble($id_produser, $data_up_produser)) {
                $data = "Failed";
            } else {
                $data = "Success";
            }

            echo $data;
            exit();
        }
    }
}