<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\AuthModel;
use App\Models\KategoriModel;
 
class Kategori extends Controller
{
	public function __construct()
	{
		helper('security');
        $this->session = session();
        $this->auth = new AuthModel();
        $this->kategori = new KategoriModel();
    }

    public function index()
    {
    	if (! $this->session->has('sess_masuk')) {
    		return redirect()->route('one');
    	} else {
    		$data['title']			=	'Kategori';
    		$data['view']			=	'admin/Kategori';
    		$data['js']				=	'script/Kategori.js';
            $data['style']          =   '';
    		$data['session_nama']	=	$this->session->get('sess_name');
    		
    		return view('template/Main', $data);
    	}
    }

    public function list()
    {
        if (! $this->session->has('sess_masuk')) {
            return redirect()->route('one');
        } else {
            $draw = intval($this->request->getGet("draw"));
            $start = intval($this->request->getGet("start"));
            $length = intval($this->request->getGet("length"));

            $query = $this->kategori->getAll();

            $data_kategori = [];

            foreach ($query->getResult() as $key) {
                if ($key->tampil == 0) {
                    $tampil = '<b style="color: green;">Enabled</b>';
                    $button = '<button
                        type="button" 
                        class="disable_kategori btn btn-block btn-outline-danger btn-xs" 
                        data-iddc="'.$key->id.'">
                        Disable
                        </button>';
                } else {
                    $tampil = '<b style="color: red;">Disabled</b>';
                    $button = '
                    <button
                        type="button"
                        id="enable_kategori"
                        class="enable_kategori btn btn-block btn-outline-success btn-xs"
                        data-iddc="'.$key->id.'">
                        Enable
                        </button>';
                }
                $data_kategori[] = array(
                    $key->nama,
                    $tampil,
                    $button
                );
            }

            $result = array(
                "draw" => $draw,
                "recordsTotal" => count($query->getResult()),
                "recordsFiltered" => count($query->getResult()),
                "data" => $data_kategori
            );

            echo json_encode($result);
            exit();
        }
    }

    public function add()
    {
        $data_add = array(
            'nama'          =>  $this->request->getPost('nama_kategori'),
            'created_by'    =>  0
        );

        if (! $this->kategori->addNew($data_add)) {
            $data = "Failed";
        } else {
            $data = "Success";
        }
        
        echo $data;
        exit();
    }

    public function enable()
    {
        $id = $this->request->getPost('id');

        $data_enable = [
            'tampil'    =>  0
        ];

        if (! $this->kategori->editAble($id, $data_enable)) {
            $data = "Failed";
        } else {
            $data = "Success";
        }
        
        echo $data;
        exit();
    }

    public function disable()
    {
        $id = $this->request->getPost('id');

        $data_disable = [
            'tampil'    =>  1
        ];

        if (! $this->kategori->editAble($id, $data_disable)) {
            $data = "Failed";
        } else {
            $data = "Success";
        }
        
        echo $data;
        exit();
    }
}