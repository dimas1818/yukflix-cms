<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\AuthModel;
use App\Models\FilmModel;
use App\Models\TransaksiModel;
 
class Transaksi extends Controller
{
	public function __construct()
	{
		helper('security');
        helper('text_helper');
        $this->session = session();
        $this->auth = new AuthModel();
        $this->transaksi = new TransaksiModel();
    }

    public function index()
    {
    	if (! $this->session->has('sess_masuk')) {
    		return redirect()->route('one');
    	} else {
    		$data['title']			=	'Transaksi';
    		$data['view']			=	'admin/Transaksi';
    		$data['js']				=	'script/Transaksi.js';
            $data['style']          =   '';
    		$data['session_nama']	=	$this->session->get('sess_name');
    		
    		return view('template/Main', $data);
    	}
    }

    public function list()
    {
        $draw = intval($this->request->getGet("draw"));
        $start = intval($this->request->getGet("start"));
        $length = intval($this->request->getGet("length"));


        $param = array('list' => 1);
        $query = $this->transaksi->getAll($param);

        $data_transaksi = [];
        $i = 1;

        foreach ($query->getResult() as $key) {
            if ($key->status == 0) {
                $status = "Belum Konfirmasi";
                $button = '<button type="button" class="konfirmasi_bayar btn btn-block btn-outline-primary btn-xs" data-idpp="'.$key->id.'"> Konfirmasi </button>';
            } else {
                $status = "Ter-Konfirmasi";
                $button = "";
            }
            $data_transaksi[] = array(
                $i++,
                $key->nama_user,
                $key->email_user,
                $key->judul,
                $key->tiket,
                $key->bayar_pakai,
                $status,
                $button
            );
        }

        $result = array(
            "draw" => $draw,
            "recordsTotal" => count($query->getResult()),
            "recordsFiltered" => count($query->getResult()),
            "data" => $data_transaksi
        );

        echo json_encode($result);
        exit();
    }

    public function konfirmasi()
    {
        $id = $this->request->getPost('id');

        $startpay = time();
        $data_konfirmasi = [
            'startpay'  => $startpay,
            'status'    =>  1
        ];

        if (! $this->transaksi->editAble($id, $data_konfirmasi)) {
            $data = "Failed";
        } else {
            $data = "Success";
        }
        
        echo $data;
        exit();
    }
}