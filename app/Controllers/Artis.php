<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\AuthModel;
use App\Models\FilmModel;
use App\Models\ArtisModel;
 
class Artis extends Controller
{
	public function __construct()
	{
		helper('security');
        helper('text_helper');
        $this->session = session();
        $this->auth = new AuthModel();
        $this->artis = new ArtisModel();
        $this->film = new FilmModel();
    }

    public function index()
    {
    	if (! $this->session->has('sess_masuk')) {
    		return redirect()->route('one');
    	} else {
    		$data['title']			=	'Aktor / Aktris';
    		$data['view']			=	'admin/Artis';
    		$data['js']				=	'script/Artis.js';
            $data['style']          =   'style/Artis.css';
    		$data['session_nama']	=	$this->session->get('sess_name');
    		
    		return view('template/Main', $data);
    	}
    }

    public function list()
    {
        if (! $this->session->has('sess_masuk')) {
            return redirect()->route('one');
        } else {
            $param  =   array('list'  => 1);
            $data   =   $this->artis->getAll($param)->getResult();

            if ($this->request->getGet('id_artis')) {
                $param =    array_merge($param, array('id' =>  $this->request->getGet('id_artis')));
                $data  =    $this->artis->getAll($param)->getRow();
            }

            echo json_encode($data);
            exit();
        }
    }

    public function add()
    {
        if (! $this->session->has('sess_masuk')) {
            return redirect()->route('one');
        } else {
            $nama_aktortris         = $this->request->getPost('nama_aktortris');
            $foto_aktortris         = $this->request->getFile('foto_aktortris');

            $data_add_artis = array(
                'nama'          => $nama_aktortris,
                'created_by'    => $this->session->get('sess_id')
            );

            if (! $add_artis = $this->artis->addNew($data_add_artis)) {
                $data = "Failed";
            } else {
                $new_id_artis = $add_artis['id'];

                // $lokasiAktortris     = ROOTPATH.'writable/uploads/images/actor-ess/';
                $lokasiAktortris    = '../assets/kinarya/poto/pemain/';

                $acak = random_string('alnum', 10).$new_id_artis;

                $createAktortris        = $lokasiAktortris.$acak;
                
                if(!mkdir($createAktortris,0777)){
                    $data = "Failed";
                }

                $ext_a = $foto_aktortris->getExtension();
                // $foto_aktortris->store('images/actor-ess/'.$acak,$new_id_artis.'.'.$ext_a);
                $foto_aktortris->move($createAktortris,$new_id_artis.'.'.$ext_a);

                $data_up_artis = array(
                    'foto'  => $acak,
                    'ext_a' => $ext_a
                );

                if (! $this->artis->editAble($new_id_artis, $data_up_artis)) {
                    $data = "Failed";
                } else {
                    $data = "Success";
                }
            }

            echo $data;
            exit();
        }
    }

    public function update()
    {
        if (! $this->session->has('sess_masuk')) {
            return redirect()->route('one');
        } else {
            $id_aktortris           = $this->request->getPost('id_artis');
            $nama_aktortris         = $this->request->getPost('nama_aktortris');
            $foto_aktortris         = $this->request->getFile('foto_aktortris');
            $foto                   = $this->request->getPost('foto');
            $ext                    = $this->request->getPost('ext');

            if ($foto_aktortris == null || $foto_aktortris == '') {
                $data_up_artis = array(
                    'nama'  => $nama_aktortris
                );
            } else {
                $lokasi     = '../assets/kinarya/poto/pemain/'.$foto.'/';
                $_old       = $lokasi.$id_aktortris.'.'.$ext.'';

                if (file_exists($_old)) {
                    if (! unlink($_old)) {
                        $data = "Failed";
                    } else {
                        $ext_a = $foto_aktortris->getExtension();
                        $foto_aktortris->move($lokasi,$id_aktortris.'.'.$ext_a);

                        $data_up_artis = array(
                            'nama'  => $nama_aktortris,
                            'ext_a' => $ext_a
                        );
                    }
                }
            }

            if (! $this->artis->editAble($id_aktortris, $data_up_artis)) {
                $data = "Failed";
            } else {
                $data = "Success";
            }

            echo $data;
            exit();
        }
    }
}