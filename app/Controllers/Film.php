<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\AuthModel;
use App\Models\FilmModel;
use App\Models\KategoriModel;
use App\Models\ProduserModel;
use App\Models\DirectorModel;
use App\Models\ArtisModel;
 
class Film extends Controller
{
	function __construct()
	{
		helper('text_helper');
        $this->session = session();
        $this->db = db_connect();
        $this->film = new FilmModel();
        $this->kategori = new KategoriModel();
        $this->produser = new ProduserModel();
        $this->director = new DirectorModel();
        $this->artis = new ArtisModel();
        // $this->session = session();
    }

    public function index()
    {
    	if (! $this->session->has('sess_masuk')) {
    		return redirect()->route('one');
    	} else {
    		$data['title']			=	'Film';
    		$data['view']			=	'admin/Film';
    		$data['js']				=	'script/Film.js';
    		$data['style']			=	'style/Film.css';
    		$data['session_nama']	=	$this->session->get('sess_name');
    		$param = array('list'	=> 1);
    		$data['film']			=	$this->film->getAll($param)->getResult();
    		$data['kategori']		=	$this->kategori->getAll()->getResult();
    		$data['produser']		=	$this->produser->getAll()->getResult();
    		$data['director']		=	$this->director->getAll()->getResult();
    		$data['artis']			=	$this->artis->getAll()->getResult();

    		// echo "<pre>";
    		// print_r($data['film']);
    		// die();
    		
    		return view('template/Main', $data);
    	}
    }

    public function list()
    {
    	if (! $this->session->has('sess_masuk')) {
    		return redirect()->route('one');
    	} else {
    		$param = array('list'	=> 1);

    		if ($this->request->getGet('id_movies')) { $param = array('list'	=>	1, 'id'	=>	$this->request->getGet('id_movies')); }

    		$data			=	$this->film->getAll($param)->getResult();

    		echo json_encode($data);
    		exit();
    	}
    }

    public function release()
    {
    	if (! $this->session->has('sess_masuk')) {
    		return redirect()->route('one');
    	} else {
    		$id = $_POST["id"];

    		$data_up = [
    			'status'	=>	1
    		];

    		if (! $this->film->editAble($id, $data_up)) {
    			$data = "Failed";
    		} else {
    			$data = "Success";
    		}

    		echo $data;
    		exit();
    	}
    }

    public function unrelease()
    {
    	if (! $this->session->has('sess_masuk')) {
    		return redirect()->route('one');
    	} else {
    		$id = $_POST["id"];

    		$data_up = [
    			'status'	=>	2
    		];

    		if (! $this->film->editAble($id, $data_up)) {
    			$data = "Failed";
    		} else {
    			$data = "Success";
    		}

    		echo $data;
    		exit();
    	}
    }

    public function add()
    {
    	$judul			= $this->request->getPost('nama_film');
    	$deskripsi 		= $this->request->getPost('deskripsi_film');
    	$id_kategori 	= $this->request->getPost('id_kategori');
    	$tahun 			= $this->request->getPost('tahun_film');
    	$durasi 		= $this->request->getPost('jam_film').':'.$this->request->getPost('menit_film').':'.$this->request->getPost('detik_film');
    	$harga_tiket 	= str_replace('.', '', $this->request->getPost('harga_tiket'));
    	$status 		= $this->request->getPost('status_film');

    	$prod 				= $this->request->getPost('prod_movies');
    	$dir 				= $this->request->getPost('dir_movies');
    	$artis1 			= $this->request->getPost('artis_movies1');
    	// $artis2 			= $this->request->getPost('artis_movies2');
    	// $artis3 			= $this->request->getPost('artis_movies3');

    	$cover_film			= $this->request->getFile('cover_film');
    	$thumbnail_film		= $this->request->getFile('thumbnail_film');
    	$trailer_film		= $this->request->getFile('trailer_film');
    	$_film				= $this->request->getFile('_film');

    	$data_add_film = array(
    		'judul'			=> $judul,
    		'deskripsi'		=> $deskripsi,
    		'id_genre'		=> $id_kategori,
    		'tahun'			=> $tahun,
    		// 'trailer'		=> $trailer_film->getName(),
    		// 'movies'		=> $_film->getName(),
    		'durasi'		=> $durasi,
    		'harga'			=> $harga_tiket,
    		'status'		=> $status,
    		'created_by'	=> $this->session->get('sess_id'),
    	);

    	if (! $add_film = $this->film->addNew($data_add_film)) {
    		$data = "Failed";
    	} else {
    		$new_id_film = $add_film['id'];
    		
    		// $lokasiMovies 	= ROOTPATH.'writable/uploads/videos/movies/';
    		$lokasiMovies 	= '../z9xmxp5/vmp9xwk/';
    		// $lokasiTrailer 	= ROOTPATH.'writable/uploads/videos/trailer/';
    		$lokasiTrailer 	= '../assets/kinarya/pideo/trailernya/';

    		// $lokasiThumbnail = ROOTPATH.'writable/uploads/images/movies/thumbnail/';
    		$lokasiThumbnail = '../assets/kinarya/poto/pilemnya/thumbnail/';
    		// $lokasiCover = ROOTPATH.'writable/uploads/images/movies/cover/';
    		$lokasiCover = '../assets/kinarya/poto/pilemnya/koper/';

    		$acak = random_string('alnum', 10).$new_id_film;
    		
    		$createMovies 	= $lokasiMovies.$acak;
    		$createTrailer 	= $lokasiTrailer.$acak;
    		
    		$createThumbnail 	= $lokasiThumbnail.$acak;
    		$createCover 		= $lokasiCover.$acak;
    		
    		if (! mkdir($createMovies,0777)) {
    			$data = "Failed";
    		}
    		if (! mkdir($createTrailer,0777)) {
    			$data = "Failed";
    		}

    		if (! mkdir($createThumbnail,0777)) {
    			$data = "Failed";
    		}
    		if (! mkdir($createCover,0777)) {
    			$data = "Failed";
    		}

    		$ext_t = $trailer_film->getExtension();
    		$ext_f = $_film->getExtension();
    		// $trailer_film->store('videos/trailer/'.$acak,$id_movies.'.'.$ext_t);
    		if (! $trailer_film->move($createTrailer,$new_id_film.'.'.$ext_t)) {
    			$data = "Failed";
    		}
    		// $_film->store('videos/movies/'.$acak,$id_movies.'.'.$ext_f);
    		if (! $_film->move($createMovies,$new_id_film.'.'.$ext_f)) {
    			$data = "Failed";
    		}

    		// $cover_film->move(ROOTPATH . 'public/movies/images');
    		// $thumbnail_film->move(ROOTPATH . 'public/movies/images');

    		$ext_th = $thumbnail_film->getExtension();
    		$ext_c = $cover_film->getExtension();
    		// $thumbnail_film->store('images/movies/thumbnail/'.$acak,$id_movies.'.'.$ext_th);
    		if (! $thumbnail_film->move($createThumbnail,$new_id_film.'.'.$ext_th)) {
    			$data = "Failed";
    		}
    		// $cover_film->store('images/movies/cover/'.$acak,$id_movies.'.'.$ext_c);
    		if (! $cover_film->move($createCover,$new_id_film.'.'.$ext_c)) {
    			$data = "Failed";
    		}

    		$data_up = array(
    			'pathvideo'		=>	$acak,
    			'videoext'		=>	$ext_f,
    			'trailer'		=>	$acak,
    			'trailerext'	=>	$ext_t,
    			'thumbnail'		=>	$acak,
    			'ext_t'			=>	$ext_th,
    			'cover'			=>	$acak,
    			'ext_c'			=>	$ext_c,
    		);

    		if (! $up = $this->film->editAble($new_id_film, $data_up)) {
    			$data = "Failed";
    		} else {
    			$data = "Success";
    		}
    	}

    	echo $data;
    	exit();
    }
}
